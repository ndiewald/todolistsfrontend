import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent {
  public name = '';
  public email = '';
  public password = '';

  constructor(
    private readonly httpClient: HttpClient,
    private readonly router: Router,
  ) {}

  onSignIn(): void {
    this.router.navigate(['signin']);
  }

  onSignUp(): void {
    this.httpClient
      .post(`${environment.backendUrl}/auth/signup`, {
        name: this.name,
        email: this.email,
        password: this.password,
      })
      .subscribe((response) => {
        console.log(response);
      });
  }
}
