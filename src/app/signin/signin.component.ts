import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
})
export class SigninComponent implements OnInit {
  public email = '';
  public password = '';

  constructor(
    private readonly httpClient: HttpClient,
    private readonly router: Router,
  ) {}

  ngOnInit(): void {}

  onSignIn(): void {
    this.httpClient
      .post(
        `${environment.backendUrl}/auth/signin`,
        {
          email: this.email,
          password: this.password,
        },
        {
          withCredentials: true,
        },
      )
      .subscribe((response) => {
        sessionStorage.setItem('isSignedIn', 'true');
        this.router.navigate(['todos']);
      });
  }

  onSignUp(): void {
    this.router.navigate(['signup']);
  }
}
