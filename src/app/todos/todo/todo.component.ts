import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Todo } from '../todo';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss'],
})
export class TodoComponent implements OnInit {
  @Input() todo: Todo = <Todo>{
    _id: '',
    title: '',
    description: '',
    userId: '',
  };
  @Output() edit = new EventEmitter<void>();
  @Output() delete = new EventEmitter<void>();

  editMode = false;

  constructor() {}

  ngOnInit(): void {
    if (!this.todo) this.todo;
  }

  onSave(): void {
    this.editMode = false;
  }

  onEdit(): void {
    this.editMode = true;
    this.edit.emit();
  }

  onDelete(): void {
    this.delete.emit();
  }
}
