import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Todo } from './todo';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss'],
})
export class TodosComponent implements OnInit {
  showAddTodoForm = false;

  title = '';
  description = '';

  todos: Todo[] = [];

  constructor(private readonly httpClient: HttpClient) {}

  ngOnInit(): void {
    this.loadTodos();
  }

  loadTodos(): void {
    this.httpClient
      .get(environment.backendUrl + '/todos', {
        withCredentials: true,
      })
      .subscribe((todos) => {
        this.todos = todos as Todo[];
      });
  }
}
