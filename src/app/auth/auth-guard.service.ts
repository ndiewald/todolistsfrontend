import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService implements CanActivate {
  constructor(private readonly router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): boolean {
    if (this.isLoggedIn()) return true;
    this.router.navigate(['signin']);
    return false;
  }

  isLoggedIn(): boolean {
    if (sessionStorage.getItem('isSignedIn') === 'true') return true;
    return false;
  }
}
